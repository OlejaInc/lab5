//
//  ViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/12/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class LogoViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
            self.performSegue(withIdentifier: "PushWelcomeView", sender: nil)
        }
    }
}
