//
//  SignUpViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/12/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameField: CustomTextField!
    @IBOutlet weak var birthdayField: CustomTextField!
    @IBOutlet weak var emailField: CustomTextField!
    @IBOutlet weak var phoneNumberField: CustomTextField!
    @IBOutlet weak var addressField: CustomTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapImage))
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }

    @objc func didTapImage() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self

        present(imagePicker, animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            print("No image found")
            return
        }

        imageView.image = image
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ConfirmSignUpViewController {
            let avatar = imageView.image?.jpegData(compressionQuality: 0.1)?.base64EncodedString()
            destinationVC.params = [
                "Base64Photo": avatar!,
                "FullName": nameField.text!,
                "Birthday": birthdayField.text!,
                "Email": emailField.text!,
                "Phone": phoneNumberField.text!,
                "Address": addressField.text!
            ]
        }
    }
}
