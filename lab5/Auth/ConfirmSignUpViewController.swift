//
//  ConfirmSignUpViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/25/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class ConfirmSignUpViewController: UIViewController {
    var params: [String: String] = [:]

    @IBOutlet weak var usernameField: CustomTextField!
    @IBOutlet weak var passwordField: CustomTextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        super.viewDidLoad()
    }

    @IBAction func didTapSignUp(_ sender: Any) {
        params["Username"] = usernameField.text!
        params["Password"] = passwordField.text!

        HTTPClient.register(with: params) { response in
            do {
                print("RESPONSE: \(response)")

                if response == "\"SUCCESS\"" {
                    self.didSignUpSuccessfully()
                } else {
                    let json = try JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: []) as! [String: String]
                    self.errorLabel.text = json["Message"]
                    self.errorLabel.isHidden = false
                    self.passwordField.text = ""
                }
            } catch {
                print("Unexpected error: \(error).")
                self.errorLabel.text = "Sign up error."
                self.errorLabel.isHidden = false
                return
            }
        }
    }

    private func didSignUpSuccessfully() {
        HTTPClient.login(email: params["Email"]!, password: params["Password"]!) { response in
            do {
                let json = try JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: []) as! [String: String]

                if json["Status"] == "SUCCESS" {
                    let token = json["Message"]
                    HTTPClient.shared = HTTPClient(token!)
                    self.performSegue(withIdentifier: "PushHomeView2", sender: self)
                }
            } catch {
                print("Unexpected error: \(error).")
                self.errorLabel.text = "Log in error."
                self.errorLabel.isHidden = false
            }
        }
    }
}
