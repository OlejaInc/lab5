//
//  LogInViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/12/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {
    @IBOutlet weak var emailField: CustomTextField!
    @IBOutlet weak var passwordField: CustomTextField!
    @IBOutlet weak var invalidCredentialsLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        super.viewDidLoad()
    }

    @IBAction func didTapLogIn(_ sender: Any) {
        activityIndicator.startAnimating()
        HTTPClient.login(email: emailField.text!, password: passwordField.text!) { response in
            do {
                self.activityIndicator.stopAnimating()
                let json = try JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: []) as! [String: String]

                if json["Status"] == "SUCCESS" {
                    let token = json["Message"]
                    HTTPClient.shared = HTTPClient(token!)
                    self.performSegue(withIdentifier: "PushHomeView", sender: self)
                } else {
                    self.invalidCredentialsLabel.isHidden = false
                    self.passwordField.text = ""
                }
            } catch {
                self.invalidCredentialsLabel.text = "Something went wrong..."
                self.invalidCredentialsLabel.isHidden = false
            }
        }
    }
}
