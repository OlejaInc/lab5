//
//  HTTPClient.swift
//  lab5
//
//  Created by Alex Hîncu on 11/12/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import Foundation

let baseURL = "http://81.180.72.17/api"
let defaultHeaders = [
    "Content-Type": "application/x-www-form-urlencoded"
]

class HTTPClient {
    static var shared = HTTPClient("test")
    let token: String

    var headers: [String: String] {
        get {
            var headers = defaultHeaders
            headers["token"] = token
            return headers
        }
    }

    init(_ token: String) {
        self.token = token
    }

    func fetchProfile(_ completionHandler: @escaping ([String: Any]) -> Void) {
        fetchJSON(from: "\(baseURL)/Profile/GetProfile") { completionHandler($0 as! [String: Any]) }
    }

    func fetchDoctorList(_ completionHandler: @escaping ([[String: Any]]) -> Void) {
        fetchJSON(from: "\(baseURL)/Doctor/GetDoctorList") { completionHandler($0 as! [[String: Any]]) }
    }

    func fetchDoctor(_ id: Int, _ completionHandler: @escaping ([String: Any]) -> Void) {
        fetchJSON(from: "\(baseURL)/Doctor/GetDoctor/\(id)") { completionHandler($0 as! [String: Any]) }
    }

    func createConsultation(with params: [String: String], completionHandler: @escaping (String) -> Void) {
        HTTPClient.post(params, to: "\(baseURL)/Doctor/AddConsultation", withHeaders: headers) { response in
            DispatchQueue.main.async { completionHandler(response) }
        }
    }

    private func fetchJSON(from url: String, _ completionHandler: @escaping (Any) -> Void) {
        var headers = defaultHeaders
        headers["token"] = token

        HTTPClient.get(from: url, withHeaders: headers) { response in
            let json = try! JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: [])
            DispatchQueue.main.async { completionHandler(json) }
        }
    }

    static func register(with params: [String: String], _ completionHandler: @escaping (String) -> Void) {
        post(params, to: "\(baseURL)/Register/UserReg", withHeaders: defaultHeaders) { response in
            DispatchQueue.main.async { completionHandler(response) }
        }
    }

    static func login(email: String, password: String, _ completionHandler: @escaping (String) -> Void) {
        let params = [
            "Email": email,
            "Password": password
        ]

        post(params, to: "\(baseURL)/Login/UserAuth", withHeaders: defaultHeaders) { response in
            DispatchQueue.main.async { completionHandler(response) }
        }
    }

    static private func get(from url: String, withHeaders headers: [String: String] = [:], _ completionHandler: @escaping (String) -> Void) {
        var request = URLRequest(url: URL(string: url)!)
        request.allHTTPHeaderFields = headers
        make(request: request, completionHandler: completionHandler)
    }

    static private func post(
        _ payload: [String: String],
        to url: String,
        withHeaders headers: [String: String] = [:],
        _ completionHandler: @escaping (String) -> Void
    ) {
        let postData = payload
            .map { (key, value) in "\(key)=\(value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? value)" }
            .joined(separator: "&")
            .data(using: String.Encoding.utf8)

        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData
        make(request: request, completionHandler: completionHandler)
    }

    static private func make(request: URLRequest, completionHandler: @escaping (String) -> Void) {
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            let string = String(data: data!, encoding: .utf8)!
            completionHandler(string)
        }
        task.resume()
    }
}
