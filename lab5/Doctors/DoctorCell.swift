//
//  DoctorCell.swift
//  lab5
//
//  Created by Alex Hîncu on 11/25/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit
import Cosmos

class DoctorCell: UITableViewCell {
    var doctor: Doctor?

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!

    func setDoctor(_ doctor: Doctor) {
        self.doctor = doctor
        nameLabel.text = doctor.name
        specialityLabel.text = doctor.speciality
        locationLabel.text = doctor.location
        pictureView.image = doctor.photo

        ratingView.settings.fillMode = .precise
        ratingView.settings.starMargin = 4
        ratingView.text = String(describing: doctor.rating)
    }
}
