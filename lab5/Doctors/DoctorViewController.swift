//
//  DoctorViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/25/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit
import Cosmos

class DoctorViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!

    var doctor: Doctor?

    override func viewDidLoad() {
        super.viewDidLoad()

        let doctor = self.doctor!
        imageView.image = doctor.photo
        nameLabel.text = doctor.name
        specialityLabel.text = doctor.speciality

        ratingView.settings.fillMode = .precise
        ratingView.settings.starMargin = 4
        ratingView.rating = doctor.rating
        ratingView.text = String(describing: doctor.rating)

        descriptionLabel.text = doctor.description
        locationLabel.text = doctor.location
    }
}
