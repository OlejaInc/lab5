//
//  DoctorTableViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/25/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class DoctorTableViewController: UITableViewController {
    private var doctors: [Doctor] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.separatorStyle = .none
        HTTPClient.shared.fetchDoctorList { list in
            DispatchQueue.main.async {
                self.doctors = list.map { Doctor($0) }
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctors.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "doctorCell", for: indexPath) as! DoctorCell
        cell.setDoctor(doctors[indexPath.row])
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? DoctorViewController {
            let selectedCell = sender as! DoctorCell
            destinationVC.doctor = selectedCell.doctor
        }
    }
}
