//
//  RequestViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/26/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class RequestViewController: UIViewController {
    @IBOutlet weak var nameField: CustomTextField!
    @IBOutlet weak var diseaseField: CustomTextField!
    @IBOutlet weak var locationField: CustomTextField!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var errorLabel: UILabel!

    private var consultation: Consultation?

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
    }

    @IBAction func didTapConfirm(_ sender: Any) {
        let allFieldsComplete = [nameField, diseaseField, locationField].allSatisfy { $0?.text != nil && $0?.text != "" }
        guard allFieldsComplete else {
            self.errorLabel.isHidden = false
            return
        }

        let params = [
            "Name": nameField.text!,
            "Disease": diseaseField.text!,
            "Address": locationField.text!,
            "Description": descriptionField.text ?? "",
        ]

        for field in [nameField, diseaseField, locationField] { field?.text = "" }
        descriptionField.text = ""

        HTTPClient.shared.createConsultation(with: params) { response in
            do {
                let json = try JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: []) as! [String: Any]
                self.consultation = Consultation(json)
                self.consultation!.fetchDoctor() { doctor in
                    self.performSegue(withIdentifier: "pushRequestConfirmation", sender: self)
                }
            } catch {
                print("Consultation error.")
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        errorLabel.isHidden = true
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? RequestConfirmationViewController {
            destinationVC.consultation = consultation
        }
    }
}
