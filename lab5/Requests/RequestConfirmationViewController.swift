//
//  RequestConfirmationViewController.swift
//  lab5
//
//  Created by Alex Hîncu on 11/26/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit
import Cosmos

class RequestConfirmationViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var diseaseLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var doctorPhotoView: UIImageView!
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var doctorSpecialityLabel: UILabel!
    @IBOutlet weak var doctorRating: CosmosView!

    var consultation: Consultation?

    override func viewDidLoad() {
        super.viewDidLoad()
        let cons = consultation!
        nameLabel.text = cons.name
        diseaseLabel.text = cons.disease
        locationLabel.text = cons.location
        descriptionLabel.text = cons.description
        doctorPhotoView.image = cons.doctor?.photo
        doctorNameLabel.text = cons.doctor?.name
        doctorSpecialityLabel.text = cons.doctor?.speciality

        doctorRating.settings.fillMode = .precise
        doctorRating.settings.starMargin = 4
        doctorRating.rating = cons.doctor!.rating
        doctorRating.text = String(describing: cons.doctor!.rating)
    }
}
