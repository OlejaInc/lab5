//
//  ProfileImageView.swift
//  lab5
//
//  Created by Alex Hîncu on 12/2/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class ProfileImageView: UIImageView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let border = CAShapeLayer()
        border.strokeColor = UIColor.gray.cgColor
        border.lineDashPattern = [3, 6]
        border.frame = self.bounds
        border.fillColor = nil
        border.path = UIBezierPath(ovalIn: self.bounds).cgPath
        self.layer.addSublayer(border)
    }
}
