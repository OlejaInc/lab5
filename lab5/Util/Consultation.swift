//
//  Consultation.swift
//  lab5
//
//  Created by Alex Hîncu on 11/26/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import Foundation

class Consultation {
    let id: Int
    let name: String
    let disease: String
    let location: String
    let description: String?
    private let doctorID: Int
    var doctor: Doctor?
    var isConfirmed: Bool

    init(_ data: [String: Any?]) {
        id = data["ConsId"] as! Int
        name = data["Name"] as! String
        disease = data["Disease"] as! String
        location = data["Address"] as! String
        description = data["Description"] as? String
        doctorID = data["DocId"] as! Int
        isConfirmed = data["IsConfirmed"] as! Bool
    }

    func fetchDoctor(_ completionHandler: @escaping (Doctor) -> Void) {
        HTTPClient.shared.fetchDoctor(doctorID) { response in
            self.doctor = Doctor(response)
            completionHandler(self.doctor!)
        }
    }
}
