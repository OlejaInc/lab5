//
//  Doctor.swift
//  lab5
//
//  Created by Alex Hîncu on 11/25/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import Foundation
import UIKit

class Doctor {
    var name: String
    var speciality: String
    var location: String
    var description: String
    var rating: Double
    var photo: UIImage

    init (_ data: [String: Any]) {
        self.name = data["FullName"] as! String
        self.speciality = data["Specs"] as! String
        self.location = data["Address"] as! String
        self.description = data["About"] as! String
        self.rating = data["Stars"] as! Double

        let imageData = Data(base64Encoded: data["Photo"] as! String, options: .init(rawValue: 0))!
        let decodedimage = UIImage(data: imageData)!
        self.photo = decodedimage
    }

    var formattedRating: String {
        get { return "⭐️ \(String(describing: rating))" }
    }
}
