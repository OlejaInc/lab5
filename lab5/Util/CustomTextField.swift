//
//  CustomTextField.swift
//  lab5
//
//  Created by Alex Hîncu on 11/19/18.
//  Copyright © 2018 TeeSeal. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
    private var hasWhitePlaceholder = false
    private var paddingSize: CGFloat = 15

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        borderWidth = 1
        borderColor = UIColor(white: 0.9, alpha: 1)
        cornerRadius = 5

        font = UIFont(name: "System", size: 12)
        frame = CGRect(
            x: frame.origin.x - paddingSize,
            y: frame.origin.y - paddingSize,
            width: frame.width + paddingSize * 2,
            height: frame.height + paddingSize * 2
        )
    }

    var padding: UIEdgeInsets {
        get { return UIEdgeInsets(top: paddingSize, left: paddingSize + 5, bottom: paddingSize, right: paddingSize + 5) }
    }

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    @IBInspectable var whitePlaceholder: Bool {
        get { return hasWhitePlaceholder }
        set {
            hasWhitePlaceholder = newValue
            if (hasWhitePlaceholder) {
                let color = UIColor(white: 1, alpha: 0.8)
                attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [.foregroundColor: color])
            }
        }
    }

    @IBInspectable var padSize: CGFloat {
        get { return paddingSize }
        set { paddingSize = newValue }
    }
}
